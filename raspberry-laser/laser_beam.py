import RPi.GPIO as GPIO  # always needed with RPi.GPIO
from time import sleep


class LaserBeam:
    def __init__(self):

        self.using = False
        self.Yellow_pin = 18
        GPIO.setmode(GPIO.BCM)  # choose BCM or BOARD numbering schemes. I use BCM
        GPIO.setup(self.Yellow_pin, GPIO.OUT)  # set GPIO 18 as an output. You can use any GPIO port
        self.p = GPIO.PWM(self.Yellow_pin, 100)  # create an object p for PWM on port 18 at 50 Hertz
        self.p.start(0)  # start the PWM on 50 percent duty cycle

    def start(self):
        self.using = True
        self.p.ChangeDutyCycle(100)

    def stop(self):
        self.p.ChangeDutyCycle(0)
        sleep(10)
        self.start()
        self.using = False

    def __del__(self):
        GPIO.cleanup()


laserbeam = LaserBeam()

if __name__ == '__main__':
    laserbeam.start()
