#!/usr/bin/python3

import RPi.GPIO as GPIO
import time
import socket
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import threading
GPIO.setmode(GPIO.BOARD)

Strobe_Host_1 = "192.168.50.70"
Strobe_Host_2 = "192.168.50.76"


class LaserBreak(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        # define the pin that goes to the circuit
        self.pin_to_circuit = 7
        self.stopped = False

    def rc_time(self):
        count = 0
        # Output on the pin for
        GPIO.setup(self.pin_to_circuit, GPIO.OUT)
        GPIO.output(self.pin_to_circuit, GPIO.LOW)
        time.sleep(0.1)

        # Change the pin back to input
        GPIO.setup(self.pin_to_circuit, GPIO.IN)

        # Count until the pin goes high
        while GPIO.input(self.pin_to_circuit) == GPIO.LOW:
            count += 1
        return count

    def run(self):
        # Catch when the script is interrupted, cleanup correctly

        # Main loop
        while not self.stopped:
            result = self.rc_time()
            print(result)
            if result >= 2000:
                print("triggered: {}".format(result))
                try:
                    self.send_alert_email()
                    self.send_cmd_laser(Strobe_Host_1, "ON")
                    self.send_cmd_laser(Strobe_Host_2, "ON")
                except Exception as e:
                    print(e)
            time.sleep(.5)

    def stop(self):
        self.stopped = True
        time.sleep(10)
        self.stopped = False
        self.start()

    def send_cmd_laser(self, host, data):
        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        port = 5052

        try:
            soc.connect((host, port))
            print("connected")
        except Exception as e:
            print("Connection error in details: {}".format(e))

        soc.sendall(data.encode("utf8") + b'--quit--')
        soc.close()
        time.sleep(1)

    def send_alert_email(self):
        """
        Send the alert in the email.
        :return:
        """
        SERVER = "smtp.office365.com"
        MY_ADDRESS = "ptkalerts@needlesolutions.com"
        PASSWORD = "Kus54394Kus54394"
        TO = ["ptkalertsgroup@needlesolutions.com"]
        # "catkinson@needlesolutions.com",
        SUBJECT = "Testing an alert for needle."
        TEXT = "Testing an alert for needle."

        # Prepare actual message
        message = """From: %s\r\nTo: %s\r\nSubject: %s\r\n\%s""" % (MY_ADDRESS, ", ".join(TO), SUBJECT, TEXT)

        server = smtplib.SMTP(SERVER, port=587)
        server.starttls()
        server.login(MY_ADDRESS, PASSWORD)

        msg = MIMEMultipart()  # create a message

        # setup the parameters of the message
        msg['From'] = MY_ADDRESS
        msg['To'] = "ptkalertsgroup@needlesolutions.com"
        msg['Subject'] = "This is testing to send an alert."

        # add in the message body
        msg.attach(MIMEText(message, 'plain'))

        # send the message via the server set up earlier.
        server.send_message(msg)
        server.quit()

    def __del__(self):
        GPIO.cleanup()


laserbreak = LaserBreak()

if __name__ == '__main__':
    laserbreak.start()
