- Install opencv

https://www.learnopencv.com/install-opencv3-on-ubuntu/


- Cmake build command

    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_OPENGL=ON -D FORCE_VTK=ON -D WITH_JPEG=ON \
          -D WITH_TBB=ON -D WITH_GDAL=ON -D WITH_XINE=ON -D BUILD_EXAMPLES=OFF -D WITH_V4L=ON -D INSTALL_C_EXAMPLES=OFF ..
          
- Install mercuryapi for python module.

    apt-get install patch xsltproc gcc libreadline-dev python-dev python-setuptools
    

- Make the server running
    
    Creating Database. 
    1. python3 manage.py db init
    2. python3 manage.py db migrate
    3. python3 manage.py db upgrade
    
    Run server
    python3 manage.py runserver
     