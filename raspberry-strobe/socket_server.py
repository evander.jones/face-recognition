#!/usr/bin/python3

import socket
import sys
import traceback
import time
from threading import Thread
import blink_strobe


def main():
    blink_strobe.blinkstrobe.start()
    start_server()


def start_server():
    port = 5052

    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    print("socket created.")

    try:
        soc.bind(("", port))
    except Exception as e:
        print("Bind failed. Error: " + str(sys.exc_info()) + str(e))
        sys.exit()

    soc.listen(10)
    print("Socket now listening.")

    while True:
        connection, address = soc.accept()
        ip, port = str(address[0]), str(address[1])
        print("Connected with " + ip + ":" + port)

        try:
            Thread(target=client_thread, args=(connection, ip, port)).start()
        except:
            print("Thread did not start.")
            traceback.print_exc()
    soc.close()


def client_thread(connection, ip, port, max_buffer_size=15120):
    is_active = True

    while is_active:
        client_input = receive_input(connection, max_buffer_size)

        if "--quit--" in client_input:
            print("Client is requesting to quit.")
            connection.close()
            print("Connection " + ip + ":" + port + " closed.")
            is_active = False


def receive_input(connection, max_buffer_size):
    client_input = connection.recv(max_buffer_size)
    client_input_size = sys.getsizeof(client_input)

    if client_input_size > max_buffer_size:
        print("The input size is greater than expected {}".format(client_input_size))
        return

    decoded_input = client_input.decode("utf8").rstrip()

    process_cmd_on_laser(decoded_input)
    # turn on the laser sensor.
    return decoded_input


def process_cmd_on_laser(input_str):
    print("Received parameters: {}".format(input_str))
    try:
        data = str(input_str)
        cmd = data.split("&")[0]
        print("command received: {}".format(cmd))
        if "ON" in cmd:
            print("ON")
            try:
                blink_strobe.blinkstrobe.do_start()
                time.sleep(10)
                blink_strobe.blinkstrobe.stop()
            except:
                pass

        elif "OFF" in cmd:
            print("OFF")
            try:
                blink_strobe.blinkstrobe.stop()
            except:
                pass

    except Exception as e:
        print(e)


if __name__ == '__main__':
    main()
