from flask import Flask
from flask_login import LoginManager
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_admin import Admin
from flask_admin.contrib.sqla import ModelView
import os
app = Flask(__name__)

cur_path = os.path.dirname(os.path.abspath(__file__))
par_path = os.path.join(cur_path, os.pardir)

login_manager = LoginManager()
login_manager.init_app(app)

db = SQLAlchemy(app)
bcrypt = Bcrypt(app)

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + par_path + '/needle.sqlite3'
app.config['SECRET_KEY'] = "1254sd15gfrf1edewg1er5fefe_ed"
app.config['FLASK_ADMIN_SWATCH'] = 'cerulean'
app.config['DEBUG'] = False

admin = Admin(app, name='Needle Solution', template_mode='bootstrap3')

from project.views import main_blueprint

app.register_blueprint(main_blueprint)

from project.models import User, WhiteList, Recognition


@login_manager.user_loader
def load_user(user_id):
    return User.query.filter(User.id == int(user_id)).first()


admin.add_view(ModelView(User, db.session))
admin.add_view(ModelView(WhiteList, db.session))
admin.add_view(ModelView(Recognition, db.session))
