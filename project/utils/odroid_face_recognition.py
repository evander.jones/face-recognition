import json
import face_recognition
import os
import pickle
import cv2
from queue import Queue
import queue
import imutils
from datetime import datetime
from threading import Thread
from project.utils import socket_client
import time
from project.models import Recognition, WhiteList
from project import db
# from project.utils.rfid_reader import *
import pytz

cur_path = os.path.dirname(os.path.abspath(__file__))
par_path = os.path.join(cur_path, os.path.pardir)
models_path = os.path.join(os.path.abspath(par_path), "pkl_models")


class proc_queue():
    """
    The class to process the queue.
    """
    @staticmethod
    def put_to_queue(q, data):
        """
        :param q:
        :param data:
        :return:
        """
        try:
            q.put_nowait(data)
        except queue.Full:
            q.empty()
            pass

    @staticmethod
    def get_from_queue(q, timeout=.1):
        """
        :param q:
        :param timeout:
        :return:
        """
        try:
            data = q.get(timeout=timeout)
        except queue.Empty:
            data = None
        return data


class RecognitionPeople:
    """
    The engine to recognize the people.
    """
    def __init__(self):
        self.img_path = os.path.join(os.path.abspath(par_path), "examples")
        self.encoding_path = os.path.join(os.path.abspath(par_path), "encodings.pickle")
        self.detection_mode = "hog"   # if pi, then "hog", elif pc, then "cnn"
        self.face_cascade = cv2.CascadeClassifier(os.path.join(models_path, "haarcascade_frontalface_default.xml"))
        # self.eyeCascade = cv2.CascadeClassifier(os.path.join(models_path, "haarcascade_eye.xml"))
        # load the known faces and embeddings
        self.data = pickle.loads(open(self.encoding_path, "rb").read())
        # self.rtsp = "rtsp://admin:FLeGngRLM46CM@96.93.160.109:556/cam/realmonitor?channel=1&subtype=0&unicast=true&proto=Onvif"
        self.rtsp = "rtsp://admin:admin123@96.93.160.109:555/ch1/sub/av_stream"
        # streaming_Q: store rgb if face is detected,
        # rec_Q: store rgb if face is detected,
        # known_Q: store the names.
        self.streaming_Q = Queue(maxsize=20)
        self.rec_Q = Queue(maxsize=10)
        self.known_Q = Queue(maxsize=100)
        self.laser_beam_back_host = "192.168.50.73"
        self.laser_break_host = "192.168.50.79"
        self.laser_beam_front_host = "192.168.50.16"
        self.stream = cv2.VideoCapture(self.rtsp)
        self.detected_timestamp = datetime.now(tz=pytz.timezone("US/Eastern"))
        # self.stream = cv2.VideoCapture(os.path.join(self.img_path, "1.mp4"))
        if not self.stream.isOpened():
            raise Exception("No Stream")

        self.grabbed, self.frame = self.stream.read()
        self.stopped = False
        self.fps = self.stream.get(cv2.CAP_PROP_FPS)
        self.width = int(self.stream.get(cv2.CAP_PROP_FRAME_WIDTH))
        self.height = int(self.stream.get(cv2.CAP_PROP_FRAME_HEIGHT))
        self.prev_frame = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
        self.prev_frame = cv2.GaussianBlur(self.prev_frame, (21, 21), 0)
        print(self.fps, self.width, self.height)

    def __del__(self):
        self.stream.release()

    def start(self):
        """
        start the thread to read frames from the video stream and run the engine to recognize the people.
        :return:
        """
        Thread(target=self.update, args=()).start()
        Thread(target=self.proc_recognize, args=()).start()
        return self

    def update(self):
        """
        Read the frame and store the frame into rec_Q to process further if faces are recognized.
        :return:
        """
        # keep looping infinitely until the thread is stopped
        while True:
            # if the thread indicator variable is set, stop the thread
            if self.stopped:
                return
                # otherwise, read the next frame from the stream
            try:
                self.grabbed, self.frame = self.stream.read()
                # self.frame = cv2.imread(os.path.join(self.img_path, "1.jpg"))
                # ###detect face engine###
                rgb = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)

                gray = cv2.GaussianBlur(rgb, (21, 21), 0)
                diff = cv2.absdiff(gray, self.prev_frame)
                num = (diff > 60).sum()
                self.prev_frame = gray
                if num > 100:
                    faces = self.face_cascade.detectMultiScale(rgb, 1.3, 5)
                    if len(faces) > 0:
                        boxes = []
                        for (x, y, w, h) in faces:
                            cv2.rectangle(self.frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
                            boxes.append((int(y), int(x + w), int(y + h), int(x)))
                        proc_queue.put_to_queue(self.rec_Q, {"frame": self.frame, "boxes": boxes})
                proc_queue.put_to_queue(self.streaming_Q, self.frame)

            except Exception as e:
                print(e)
                continue

    def stop(self):
        """
        Cease the loop function to read the frame from the camera.
        :return:
        """
        # indicate that the thread should be stopped
        self.stopped = True

    def proc_streaming_marked(self):
        """
        Return the jpeg image with bounded rect.
        :return:
        """
        img = proc_queue.get_from_queue(self.streaming_Q)
        if img is not None:
            blob_ret, blob_jpeg = cv2.imencode('.jpg', img)
            # convert bytes data
            result = blob_jpeg.tobytes()
            return result
        return None

    def proc_recognize(self):
        """
        Recognize
        :param rgb:
        :param boxes:
        :return:
        """
        # initialize the list of names for each face detected
        while True:
            try:
                json_data = proc_queue.get_from_queue(self.rec_Q)
                if json_data is not None:
                    img = json_data["frame"]
                    boxes = json_data["boxes"]
                    # boxes = face_recognition.face_locations(rgb, model=self.detection_mode)
                    rgb = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
                    names = []
                    encodings = face_recognition.face_encodings(rgb, boxes)
                    # loop over the facial embeddings
                    for encoding in encodings:
                        # attempt to match each face in the input image to our known
                        # encodings
                        matches = face_recognition.compare_faces(self.data["encodings"], encoding)
                        name = "Unknown"

                        # check to see if we have found a match
                        if True in matches:
                            # find the indexes of all matched faces then initialize a
                            # dictionary to count the total number of times each face
                            # was matched
                            matchedIdxs = [i for (i, b) in enumerate(matches) if b]
                            counts = {}
                            # loop over the matched indexes and maintain a count for

                            # each recognized face face
                            for i in matchedIdxs:
                                name = self.data["names"][i]
                                counts[name] = counts.get(name, 0) + 1
                            # determine the recognized face with the largest number of
                            # votes (note: in the event of an unlikely tie Python will
                            # select first entry in the dictionary)
                            name = max(counts, key=counts.get)
                        # update the list of names
                        names.append(name)
                    if len(names) > 0:
                        ts = datetime.now(tz=pytz.timezone("US/Eastern"))
                        # print("At {}, recognized {}".format(ts, names))
                        if self.compare_timestamp(ts):
                            for name in names:
                                # name is "Unknown", then it will start to read the RFID card using RFID Reader.
                                people_list = WhiteList.query.filter_by(username=name.replace("-", " "), block=False).all()
                                if name != "UnKnown" and len(people_list) > 0:
                                    try:
                                        socket_client.send_cmd_laser(self.laser_break_host, "OFF")
                                        # socket_client.send_cmd_laser(self.laser_beam_back_host, "OFF")
                                        socket_client.send_cmd_laser(self.laser_beam_front_host, "OFF")
                                    except Exception as e:
                                        print(e)
                                elif name != "UnKnown" and len(people_list) == 0:
                                    name = "UnKnown"
                                print("people in whitelist is {}".format(people_list))

                                recog_data = Recognition(username=name, timestamp=str(ts), flag="False")
                                db.session.add(recog_data)
                                db.session.commit()

            except Exception as e:
                print(e)
                continue
                # proc_queue.put_to_queue(self.known_Q, names)
                # # loop over the recognized faces
                # for ((top, right, bottom, left), name) in zip(boxes, names):
                #     # draw the predicted face name on the image
                #     cv2.rectangle(image, (left, top), (right, bottom), (0, 255, 0), 2)
                #     y = top - 15 if top - 15 > 15 else top + 15
                #     cv2.putText(image, name, (left, y), cv2.FONT_HERSHEY_SIMPLEX,
                #                 0.75, (0, 255, 0), 2)
                # # show the output image
                # cv2.imshow("Image", image)
                # cv2.waitKey(0)

    def compare_timestamp(self, timestamp):
        """
        Compare the timestamps.
        :param timestamp:
        :return:
        """
        delta = (timestamp - self.detected_timestamp).total_seconds()
        self.detected_timestamp = timestamp
        if delta > 1:
            return True
        return False


recognition = RecognitionPeople().start()
