import socket
import time


def send_cmd_laser(host, data):
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    port = 5052

    try:
        soc.connect((host, port))
        print("connected")
    except Exception as e:
        print("Connection error in details: {}".format(e))

    soc.sendall(data.encode("utf8") + b'--quit--')
    soc.close()
    time.sleep(1)


if __name__ == '__main__':
    send_cmd_laser("ON")
