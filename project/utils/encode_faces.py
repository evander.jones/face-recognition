from imutils import paths
import face_recognition
import argparse
import pickle
import cv2
import os

cur_path = os.path.dirname(os.path.abspath(__file__))
par_path = os.path.join(cur_path, os.path.pardir)

dataset_path = os.path.join(os.path.abspath(par_path), "dataset")
encoding_path = os.path.join(os.path.abspath(par_path), "encodings.pickle")
detection_mode = "hog"   # if pi, then "hog", elif pc, then "cnn"


def encoding_faces():
	print("[INFO] quantifying faces...")
	imagePaths = list(paths.list_images(dataset_path))
	# initialize the list of known encodings and known names
	knownEncodings = []
	knownNames = []

	# loop over the image paths
	for (i, imagePath) in enumerate(imagePaths):
		# extract the person name from the image path
		print("processing {}".format(imagePath))
		print("[INFO] processing image {}/{}".format(i + 1, len(imagePaths)))
		name = imagePath.split(os.path.sep)[-2]
		# load the input image and convert it from RGB (OpenCV ordering)
		# to dlib ordering (RGB)
		image = cv2.imread(imagePath)
		rgb = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
		# detect the (x, y)-coordinates of the bounding boxes
		# corresponding to each face in the input image
		boxes = face_recognition.face_locations(rgb, model=detection_mode)
		# compute the facial embedding for the face
		encodings = face_recognition.face_encodings(rgb, boxes)

		# loop over the encodings
		for encoding in encodings:
			# add each encoding + name to our set of known names and
			# encodings
			knownEncodings.append(encoding)
			knownNames.append(name)

	# dump the facial encodings + names to disk
	print("[INFO] serializing encodings...")
	data = {"encodings": knownEncodings, "names": knownNames}
	f = open(encoding_path, "wb")
	f.write(pickle.dumps(data))
	f.close()

	return True


if __name__ == '__main__':
	encoding_faces()
