import mercury
from project.utils.socket_client import *

# Reference code:
# https://github.com/gotthardp/python-mercuryapi


class RFIDReader:
    def __init__(self):
        self.reader = None
        self.reader = mercury.Reader("tmr:///dev/ttyUSB0")
        if self.reader is None:
            raise Exception("No RFID Reader connected.")
        self.region = "NA"
        self.protocol = "GEN2"
        self.tags = []
        self.reading_timeout = 2
        self.cmd_on = "ON"
        self.cmd_off = "OFF"
        # setup
        self.set_reader()

    def set_reader(self):
        """
        Set up the RFID Reader
        :return:
        """

        # setting region
        self.reader.set_region(self.region)
        # setting antenna and protocol
        self.reader.set_read_plan([1], self.protocol)
        # getting the model
        print(self.reader.get_model())
        # checking how many antennas does it have.
        print(self.reader.get_antennas())
        # setting read power for each antenna.
        print(self.reader.get_read_powers())
        # Lists supported radio power range, in centid Bm.
        print(self.reader.get_power_range())

    def run_read(self):
        """
        Read the information in the RFID tag.
        :return:
        """
        # Performs a synchronous read, and then returns a list of TagReadData objects resulting from the search.
        # If no tags were found then the list will be empty.
        self.tags = self.reader.read(timeout=self.reading_timeout)
        send_cmd_laser(self.cmd_on)
        if len(self.tags) > 0:
            print(self.tags)
            # compare EPC number with the database.
            # if matched
            # send_cmd_laser("OFF")
            # else not matched
            # send_cmd_laser("ON")
        else:
            # email notification and turn the laser beam on.
            send_cmd_laser(self.cmd_on)

    def run_write(self, old_epc, new_epc):
        """
        Write the information to the RFID tag.
        :return:
        """
        if self.reader.write(epc_target=old_epc, epc_code=new_epc):
            print('Rewrited "{}" with "{}"'.format(old_epc, new_epc))
            return True
        else:
            print('Failed writing "{}" with "{}"'.format(old_epc, new_epc))
            return False

    def callback(self):
        pass

    def run_start_reading(self):
        """
        Starts asynchronous reading. It returns immediately and begins a sequence of reads or a continuous read.
        :return:
        """
        try:
            self.reader.start_reading(self.callback, on_time=250, off_time=0)
            return True
        except Exception as e:
            print(e)
            return False

    def run_stop_reading(self):
        """
        Stops the asynchronous reading started by reader.start_reading().
        :return:
        """
        try:
            self.reader.stop_reading()
            return True
        except Exception as e:
            print(e)
            return False


rfid_reader = RFIDReader()


if __name__ == '__main__':
    rfid_reader.run_read()
