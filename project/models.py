from project import db


# Flask and Flask-SQLAlchemy initialization here


class User(db.Model):
    """
    User table
    """
    __tablename__ = 'user'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True)
    password = db.Column(db.String(255))

    def __init__(self, username="", email="", password=""):
        self.username = username
        self.email = email
        self.password = password

    def is_active(self):
        return True

    def is_authenticated(self):
        return True

    def get_id(self):
        return self.id

    def __repr__(self):
        return '<User %r>' % self.username


class Recognition(db.Model):
    """
    Recognition table
    """
    __tablename__ = 'recognition'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), nullable=True)
    timestamp = db.Column(db.String(80), nullable=True)
    flag = db.Column(db.String(10), nullable=True)


class WhiteList(db.Model):
    """
    White list and Black list
    """
    __tablename__ = "peoplelist"
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), nullable=True)
    timestamp = db.Column(db.DateTime, nullable=True)
    block = db.Column(db.Boolean, nullable=True)


class RecordList(db.Model):
    """
    The records for storing the action of unknown person.
    """
    __tablename__ = "recordlist"
    id = db.Column(db.Integer, primary_key=True)
    phonenumber = db.Column(db.String(80), nullable=True)
    timestamp = db.Column(db.DateTime, nullable=True)
