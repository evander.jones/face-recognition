from flask import render_template, Response, request, redirect, url_for, Blueprint, flash
from flask_login import login_required, login_user, logout_user
from project.forms import RegisterForm, LoginForm
from project import db, bcrypt, login_manager
from project.models import User, Recognition, RecordList
from project.utils.odroid_face_recognition import *
from project.utils.rfid_reader import *
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


cur_path = os.path.dirname(os.path.abspath(__file__))
data_path = os.path.join(cur_path, "dataset")
basic_credentials = ("ptkalerts@needlesolutions.com", "Kus54394")

NoneType = type(None)
main_blueprint = Blueprint('main', __name__, )


@main_blueprint.route('/')
@login_required
def index():
    """
    Returns 'index.html' page in route '/'
    :return:
    """
    return render_template('index.html')


@main_blueprint.route('/register', methods=['GET', 'POST'])
def register():
    """
    Returns 'register.html' page in route '/register'
    Redirects 'login.html' page after stored new username and password when request method is post.
    :return:
    """
    register_form = RegisterForm(request.form)
    if request.method == 'POST' and register_form.validate_on_submit():
        user = User(register_form.username.data, register_form.email.data,
                    bcrypt.generate_password_hash(register_form.password.data).decode('utf - 8'))
        db.session.add(user)
        db.session.commit()

        return redirect(url_for('main.login'))
    return render_template('register.html', form1=register_form)


@main_blueprint.route('/login', methods=['GET', 'POST'])
def login():
    """
    Returns 'login.html' page in route '/login'.
    Redirects the 'index.html' page or next url when login is successful.
    :return:
    """
    login_form = LoginForm(request.form)
    if request.method == 'POST' and login_form.validate():
        user = User.query.filter_by(email=login_form.email.data).first()
        if user and bcrypt.check_password_hash(user.password, login_form.password.data):
            login_user(user)
            next = request.args.get('next')
            return redirect(next or url_for('main.index'))

        else:
            flash('Invalid email and/or password.', 'danger')
            return render_template('login.html', login_form=login_form)

    return render_template('login.html', login_form=login_form)


@main_blueprint.route('/api/recv_number')
@login_required
def recv_phone_number():
    """
    Receive the phone number
    :return:
    """
    pass


@main_blueprint.route('/api/check_recognized')
@login_required
def check_recognized():
    """
    Simultaneously checking the person recognized
    :return:
    """

    json_data = []
    recognized_person = Recognition.query.filter_by(flag="False").all()
    previous_name = None
    if len(recognized_person):
        for item in recognized_person:
            if previous_name == item.username:
                continue
            json_data.append({
                "name": item.username,
                "timestamp": item.timestamp,
                "img_path": "/static/dataset/" + item.username + "/1.jpg"
            })
            item.flag = "True"
            db.session.commit()
            previous_name = item.username
        dumped_data = json.dumps({"content": json_data})
        return dumped_data
    else:
        dumped_data = json.dumps({"content": None})
        return dumped_data


@main_blueprint.route('/logout')
@login_required
def logout():
    """
    Returns 'login.html' page after user is logged out.
    :return:
    """
    logout_user()
    flash('You were logged out.', 'success')
    return redirect(url_for('main.login'))


def send_alert_email():
    """
    Send the alert in the email.
    :return:
    """
    SERVER = "smtp.office365.com"
    MY_ADDRESS = "ptkalerts@needlesolutions.com"
    PASSWORD = "Kus54394Kus54394"
    TO = ["ptkalertsgroup@needlesolutions.com"]
    # "catkinson@needlesolutions.com",
    SUBJECT = "Testing an alert for needle."
    TEXT = "Testing an alert for needle."

    # Prepare actual message
    message = """From: %s\r\nTo: %s\r\nSubject: %s\r\n\%s""" % (MY_ADDRESS, ", ".join(TO), SUBJECT, TEXT)

    server = smtplib.SMTP(SERVER, port=587)
    server.starttls()
    server.login(MY_ADDRESS, PASSWORD)

    msg = MIMEMultipart()  # create a message

    # setup the parameters of the message
    msg['From'] = MY_ADDRESS
    msg['To'] = "ptkalertsgroup@needlesolutions.com"
    msg['Subject'] = "This is testing to send an alert."

    # add in the message body
    msg.attach(MIMEText(message, 'plain'))

    # send the message via the server set up earlier.
    server.send_message(msg)
    server.quit()


def gen():
    """
    Start the thread to receive the streaming data from socket server and returns the image processed.
    :return:
    """
    while True:
        frame = recognition.proc_streaming_marked()
        if frame is not None:
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n\r\n')


@main_blueprint.route('/video_feed')
@login_required
def video_feed():
    """
    Returns the image data as bytes.
    :return:
    """
    return Response(gen(),
                    mimetype='multipart/x-mixed-replace; boundary=frame')


@main_blueprint.route('/register_people', methods=['GET', 'POST'])
@login_required
def register_people():
    """
    Returns the image data as bytes.
    :return:
    """
    return render_template('register_people.html')


@main_blueprint.route("/events_list", methods=["GET", "POST"])
@login_required
def read_rfid():
    """
    Read rfid tag.
    :return:
    """
    events_all = Recognition.query.all()
    return render_template('events_list.html', events=events_all)


@main_blueprint.route("/write_rfid", methods=["GET", "POST"])
@login_required
def write_rfid():
    """
    Write data to rfid tag.
    :return:
    """
    pass


@login_manager.unauthorized_handler
def unauthorized_callback():
    """
    Redirects the login page when user is unauthorized.
    :return:
    """
    return redirect('/login?next=' + request.path)

